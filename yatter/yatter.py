#! /usr/bin/env python3
import yaml
import argparse
import re
import pylatex
import random
import sys
import os

from copy import deepcopy
from pathlib import Path
from PIL import Image
from subprocess import CalledProcessError
from pylatex.base_classes import Environment
from pylatex.utils import escape_latex
from pylatex.utils import bold as boldtext
from pylatex import Command, NoEscape, Section, Subsection, Subsubsection, Package, NewLine, LineBreak
from pylatex.base_classes.command import Arguments

try:
    from yaml import CSafeLoader as Loader
except ImportError:
    from yaml import Loader

# global parameters
textwidth = 7.5
multimargin = 1.5
topmargin = "1.5in"
biggap = "0.5in"
smallgap = "0.1in"
default_value = 3
default_sec_weight = 1
default_quest_val = 1
args = None


TFC_instructions = """Indicate whether the statement is true or false by marking it “TRUE” or “FALSE”;
additionally, if the statement is false, please correct it.
"""
TF_instructions = """Indicate whether the statement is true or false by marking it “TRUE” or “FALSE”."""
SA_instructions = """Answer the question in 5 sentences or less."""

# global variables
graphics_directory = 'graphics-pool'
tex = None


# custom exceptions:
class PoolExhausted(RuntimeError):
    pass


class DuplicateYAMLKeyError(RuntimeError):
    pass


# LaTeX environments:
class TexEnvs(object):
    class header(pylatex.base_classes.ContainerCommand):
        pass

    class footer(pylatex.base_classes.ContainerCommand):
        pass

    class TabularStar(pylatex.table.Tabular):

        _latex_name = 'tabular*'

    class tabularx(Environment):
        pass

    class adjustbox(Environment):
        packages = [Package('adjustbox')]

    class samepage(Environment):
        pass

    class floatrow(Environment):
        pass

    class figure(Environment):
        pass

    class minipage(Environment):
        pass

    class coverpages(Environment):
        pass

    class center(Environment):
        pass

    class parts(Environment):
        pass

    class subparts(Environment):
        pass

    class multicols(Environment):
        packages = [Package('multicol')]

    class multicols2(multicols):
        def __init__(self):
            super().__init__(options=[], arguments=[2])
            self.latex_name = 'multicols'

    class multicols3(Environment):
        def __init__(self):
            super().__init__(options=[], arguments=[3])
            self.latex_name = 'multicols'

    class easylist(Environment):
        packages = [Package('easylist', ['sharp'])]

    class questions(Environment):
        pass


class PoolName(str):

    @property
    def depth(self):
        return len(self.split('.'))

    def __lt__(self, other):
        if self.depth == other.depth:
            return str(self) < str(other)

        if self.depth < other.depth:
            return False

        else:
            return True

    def __gt__(self, other):
        if self.depth == other.depth:
            return str(self) > str(other)

        if self.depth > other.depth:
            return False

        else:
            return True


# special loader with duplicate key checking
class UniqueKeyLoader(yaml.SafeLoader):
    def construct_mapping(self, node, deep=False):
        mapping = []
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            if key not in mapping:
                mapping.append(key)
            else:
                raise DuplicateYAMLKeyError('Cannot use the same YAML key twice!!!', str(key_node.start_mark))

        return super().construct_mapping(node, deep)


# top-level functions
def included(problem, inclusion_set):
    if not isinstance(problem, str):
        problem = str(problem)
    if not isinstance(inclusion_set, list):
        inclusion_set = [inclusion_set]

    problem = problem.split('.')
    inclusion_set = [str(s).split('.') for s in inclusion_set]

    for i in inclusion_set:
        d = len(i)
        if all([p == s for p, s in zip(problem[:d], i[:d])]):
            return True
        else:
            continue
    return False


def parse_type(string):
    if string.lower() in ['tf', 'true-false', 'true/false', 'true_false']:
        return 'tf'
    if string.lower() in ['sa', 'short-answer', 'short_answer']:
        return 'sa'
    if string.lower() in ['lf', 'long-form', 'long_form']:
        return 'lf'


def getInstructions(question):
    result = ''
    if isinstance(question, dict):
        if 'prompt' in question and question['prompt']:
            result += question['prompt']
        if 'instructions' in question and question['instrucitons']:
            result += question['instructions']
        if 'additional-instructions' in question and question['additional-instructions']:
            result += ' ' + question['additional-instructions']

        if not result:
            if parse_type(question['question-type']) == 'tf':
                result += TF_instructions
            elif parse_type(question['question-type']) == 'tfc':
                result += TFC_instructions
            elif parse_type(question['question-type']) == 'sa':
                result += SA_instructions

    elif isinstance(question, str):
        # it's the type
        if parse_type(question) == 'tf':
            result += TF_instructions
        elif parse_type(question) == 'tfc':
            result += TFC_instructions
        elif parse_type(question) == 'sa':
            result += SA_instructions

    return result


def get_width(child):
    if 'graphic' not in child:
        return textwidth
    if 'width' in child:
        w = child['width']
    else:
        im = Image.open(child['graphic'])
        w = im.size[0] / im.info['dpi'][0]
    return float(re.sub('[^0-9.]', '', str(w)))


def process_buffer(buffer, seperation):
    global tex

    if len(buffer) > 1:
        ws = [get_width(item) for item in buffer]
        for i, w in enumerate(ws):
            if i == 0 or i == len(ws) - 1:
                ws[i] += multimargin / 2
            else:
                ws[i] += multimargin

        with tex.create(TexEnvs.figure(options=['H'])):
            tex.append(NoEscape(r'\centering'))
            with tex.create(TexEnvs.floatrow()):
                for item, width in zip(buffer, ws):
                    # with tex.create(TexEnvs.minipage(arguments=['{}in'.format(width + multimargin)])):
                    # tex.append(NoEscape(r'\part[{}]'.format(item['value'])))
                    # tex.append(NoEscape(r'\centering'))
                    tex.append(NoEscape(typeset_question(item)))
            # tex.append(NewLine())

    elif len(buffer) == 1:
        tex.append(NoEscape(r'\part[{}] {}'.format(buffer[0]['value'], typeset_question(buffer[0]))))


def split_prompt(prompt, section):
    sets = []
    children = getchildren(prompt, section)

    # assume they're all graphical and try to bundle them
    while children:
        buffer = []
        comb_w = 0

        while True:
            if len(children) == 0:
                break

            child = children[0]
            if 'value' not in child:
                child['value'] = default_value

            child_w = get_width(child) + 1.5

            if not buffer:
                buffer += [child]
                comb_w += child_w
                del children[children.index(child)]
                continue

            if comb_w + multimargin + child_w <= textwidth:
                buffer += [child]
                comb_w += child_w + multimargin
                del children[children.index(child)]
                continue
            else:
                break

        sets.append(buffer)
    return sets


def typeset_row(row, section):
    # all questions in minipages to keep page break from seperating question from answer space
    with tex.create(TexEnvs.adjustbox(arguments=[NoEscape(r'pagecenter')])):
        with tex.create(TexEnvs.minipage(arguments=[NoEscape(r'0.9\textwidth')])):
            process_buffer(row, section['item-seperation'])
    tex.append(NoEscape(r"""\parbox[t][{}]{{\textwidth}}{{}}""".format(section['item-seperation'])))
    tex.append(NoEscape('\par'))


def typeset_section(section):
    global tex

    if 'questions' not in section or not section['questions']:
        return

    # tex.append(Command('needspace', arguments=['3in']))
    tex.append(NoEscape(r'\vspace{{{}}}'.format(biggap)))
    with tex.create(TexEnvs.minipage(arguments=[NoEscape(r'0.9\textwidth')])):
        tex.append(NoEscape(r'\question'))
        tex.append(Section(section['title'], numbering=False))
        tex.append(NoEscape(r'\par'))
        if 'additional-instructions' in section and section['additional-instructions']:
            tex.append(boldtext(NoEscape(section['additional-instructions'])))

        prompts = list(set([x['prompt'] for x in section['questions']]))
        prompt = prompts[0]
        rows = split_prompt(prompt, section)
        tex.append(boldtext(NoEscape(prompt))) if prompt else None
        tex.append(NoEscape(r'\nopagebreak\par'))
        tex.append(NoEscape(r"""\parbox[t][{}]{{\textwidth}}{{}}""".format(smallgap)))

    tex.append(NoEscape(r'\nopagebreak\par'))
    with tex.create(TexEnvs.parts()):
        for r in rows[:]:
            typeset_row(r, section)

    for prompt in prompts[1:]:
        rows = split_prompt(prompt, section)
        with tex.create(TexEnvs.minipage(arguments=NoEscape(r'0.9\textwidth'))):
            tex.append(boldtext(NoEscape(prompt)))
            tex.append(NoEscape(r'\nopagebreak\par'))
            tex.append(NoEscape(r"""\parbox[t][{}]{{\textwidth}}{{}}""".format(smallgap)))
        tex.append(NoEscape(r'\nopagebreak\par'))
        with tex.create(TexEnvs.parts()):
            for r in rows[:]:
                typeset_row(r, section)


def getchildren(prompt, section):
    children = []
    for q in section['questions']:
        if q['prompt'] == prompt:
            children += [q]
    return children


def prune_pools(pools):
    while True:
        terminate = True
        for secn, sec in pools.items():
            if not sec:
                del pools[secn]
                terminate = False
                break
            for st, qs in sec.items():
                if not qs:
                    del pools[secn][st]
                    terminate = False
                    break
                if isinstance(qs, list):
                    for qi, q in enumerate(qs):
                        if q is None:
                            del pools[secn][st][qi]
                            terminate = False
                            break
                        if 'questions' in q:
                            if q['questions'] is None:
                                del pools[secn][st][qi]
                                terminate = False
                                break
                            for ci, c in enumerate(q['questions']):
                                if c is None:
                                    del pools[secn][st][qi]['questions'][ci]
                                    terminate = False
                                    break
                elif isinstance(qs, dict) and 'questions' in qs:
                    if qs['questions'] is None:
                        del pools[secn][st]
                        terminate = False
                        break
                    for qi, q in enumerate(qs['questions']):
                        if not q:
                            del pools[secn][st]['questions'][qi]
                            terminate = False
                            break
                        # if 'prompt' not in qs and ('prompt' not in q or q['prompt'] is None):
                        #     del pools[secn][st]['questions'][qi]
                        #     terminate = False
                        #     break
                else:
                    raise NotImplementedError
        if terminate:
            return pools


def typeset_question(quest, child=False):
    if isinstance(quest, dict):
        if 'graphic' in quest:
            gpath = quest['graphic']
            if not os.path.isfile(gpath):
                raise FileNotFoundError('could not find graphics file in pool: {}'.format(gpath))

            opts = {}
            if 'width' in quest:
                opts.update({'width': quest['width']})
            if 'height' in quest:
                opts.update({'height': quest['height']})

            return NoEscape(r'\ffigbox{{{}}}{{\fbox{{\includegraphics[keepaspectratio=true,{}]{{{}}}}}}}'.format(
                NoEscape(r'\part[' + str(quest['value']) + ']'),
                ','.join(['='.join([k, v]) for k, v in opts.items()]),
                str(gpath)))

        elif 'text' in quest:
            return NoEscape(quest['text'])

    elif isinstance(quest, str):
        if child:
            return NoEscape('{}'.format(escape_latex(quest)))
        else:
            return NoEscape('{}'.format(escape_latex(quest)))


def make_coverpage(plan):
    global tex

    cp = TexEnvs.coverpages()

    cp.append(NoEscape(r'\maketitle'))

    with cp.create(TexEnvs.center()):
        cp.append(plan['course-number'] + ': ' + plan['course-title'])
        cp.append(NoEscape(r'\\'))
        cp.append(NoEscape(r'\vspace{0.6in}'))
        # cp.append(NoEscape(r"""\begin{minipage}{0.9\textwidth}"""))
        with cp.create(TexEnvs.minipage(arguments=[NoEscape(r'0.9\textwidth')])):
            cp.append(NoEscape(plan['test-instructions']))
        # cp.append(NoEscape(r'\end{minipage}'))
        cp.append(NoEscape(r'\\'))
        cp.append(NoEscape(r'\vspace{0.6in}'))
        cp.append(NoEscape(r"""\fbox{\framebox[\textwidth]{"""))
        # cp.append(NoEscape(r"""\begin{minipage}{0.9\textwidth}"""))
        with cp.create(TexEnvs.minipage(arguments=[NoEscape(r'0.9\textwidth')])):
            cp.append(NoEscape(r'\vspace{0.1in}'))
            cp.append(NoEscape(plan['honor-statement']))
            cp.append(NoEscape(r'\vspace{0.1in}'))
        # cp.append(NoEscape(r'\end{minipage}'))
        cp.append(NoEscape(r'}}'))

        cp.append(NoEscape(r'\vfill'))

        with cp.create(TexEnvs.minipage(options=['c'], arguments=[NoEscape(r'0.9\textwidth')])):
            cp.append(Subsubsection('Test Organization', numbering=False))
            with cp.create(TexEnvs.easylist()):
                cgt = 0
                pgt = 0
                for sec, probs in zip(plan['sections'], plan):
                    pt = 0
                    ct = 0
                    for p in sec['questions']:
                        ct += 1
                        pt += sec['weight'] * p['value']
                    cgt += ct
                    pgt += pt
                    cp.append(NoEscape(r'#   ' + sec['title']+' -- ' + str(ct) + ' questions totalling ' + str(pt) + ' points.'))
            cp.append(boldtext(NoEscape(r'TOTAL POINTS: {}'.format(pgt))))
            cp.append(NoEscape(r'\vspace{0.9in}'))
    return cp


def makeHeader(todo):
    global tex
    # Add document header
    tex.preamble.append(Command('pagestyle', 'headandfoot'))
    t1 = TexEnvs.TabularStar(
        start_arguments=[NoEscape(r'\textwidth')],
        table_spec=r'@{\extracolsep{\fill}}lcr'
    )
    t1.add_row([
        NoEscape(todo['course-number']),
        todo['title'],
        todo['test-date'],
    ])
    tr2 = pylatex.table.Tabularx(
        width_argument=NoEscape(r'\textwidth'),
        table_spec='lXr',
    )

    tr2.add_row([
        NoEscape(r'\makebox[0.5\textwidth]{Name:\enspace\hrulefill}'),
        NoEscape(r''),
        NoEscape(r'\makebox[0.3\textwidth]{ID\#:\enspace\hrulefill}'),
    ])

    tex.preamble.append(TexEnvs.header(arguments=['',
                                     NoEscape(
                                         t1.dumps()
                                         + '%\n'
                                         + r'\vspace{0.1in}'
                                         + '%\n'
                                         + pylatex.NewLine().dumps()
                                         + '%\n'
                                         + tr2.dumps()),
                                     '']
                                     )
    )

    tex.preamble.append(TexEnvs.footer(arguments=[
        '',
        '',
        NoEscape(r'\thepage\hspace{0.7ex}of\hspace{0.7ex}\numpages')
        ]
    ))

    # tex.preamble.append(Command('firstpageheader', [NoEscape(todo['course-number']), '', NoEscape(todo['course-title'])]))


def flatten_top(iterb, index=None, value=default_value):
    result = []
    if isinstance(iterb, list):
        for i, item in enumerate(iterb):
            result += flatten_top(item, index=i, value=value)

    elif isinstance(iterb, dict):
        if 'value' not in iterb:
            iterb['value'] = value

        if 'questions' in iterb:
            # terminal condition... no recurse
            for qi, q in enumerate(iterb['questions']):
                if q is not None:
                    result += [(index, qi, q)]

            return result

        for k, item in iterb.items():
            if item is not None:
                result += flatten_top(item, iterb['value'])

    return result


def plan_section(section, pool_name, pools, variants):

    def do_variant(pi, qi, q):
        try:
            p_choice = variants[(pool_name, section['question-type'], pi, qi)]
        except:
            p_choice = []
        if len(p_choice) < len(q['variants']):
            v_choice = random.choice([ind for ind in range(len(q['variants'])) if ind not in p_choice])
        else:
            v_choice = random.choice(range(len(q['variants'])))
        q['text'] = q['variants'][v_choice]
        variants.update({(pool_name, section['question-type'], pi, qi): p_choice + [v_choice]})

    secplan = []

    for i, p in enumerate(section['pools']):
        if not included(pool_name, p['included-sections']):
            return

        try:
            cands = pools[pool_name][section['question-type']]

        except KeyError:
            return

        cands = flatten_top(cands)

        if p['quantity'] == -1:
            for pi, qi, q in cands:
                if 'value' not in q or q['value'] is None:
                    q['value'] = default_value
                q['value'] *= p['weight']
                if 'variants' in q:
                    do_variant(pi, qi, q)
                secplan += [q]
                pools[pool_name][section['question-type']][pi]['questions'][qi] = None
        else:
            for j in range(p['quantity']):
                if not cands:
                    raise PoolExhausted
                q = random.choice(cands)
                if 'variants' in q:
                    do_variant(*q)
                secplan += [q]
                cands.remove(secplan[-1])
                pools[pool_name][section['question-type']][pi]['questions'][qi] = None
        prune_pools(pools)

    section['questions'] += secplan
    return variants


def stdize_pool_spec(p, sec):
    # do stuff to pool_spec
    if not isinstance(p['included-sections'], list):
        p['included-sections'] = [p['included-sections']]
    if 'prompt' not in p or not p['prompt']:
        p['prompt'] = None
    if 'weight' not in p or (not p['weight'] and p['weight'] not in [0, '0']):
        p['weight'] = 1
    p['weight'] *= sec['weight']
    p['included-sections'] = [str(p) for p in p['included-sections']]


def stdize_sec_spec(sec_spec, exam_spec):
    # do stuff to section_spec
    sec_spec['questions'] = []
    sec_spec['question-type'] = parse_type(sec_spec['question-type'])

    if 'item-seperation' not in sec_spec or not sec_spec['item-seperation']:
        sec_spec['item-seperation'] = "0.1in"

    if 'prompt' not in sec_spec or not sec_spec['prompt']:
        sec_spec['prompt'] = getInstructions(sec_spec['question-type'])

    if 'weight' not in sec_spec or not sec_spec['weight']:
        sec_spec['weight'] = 1

    if 'quantity' not in sec_spec or not sec_spec['quantity']:
        sec_spec['quantity'] = -1

    if 'pools' not in sec_spec or not sec_spec['pools']:
        assert 'included-sections' in sec_spec
        sec_spec['pools'] = [{'included-sections': sec_spec['included-sections']}]
        sec_spec['pools'][0]['weight'] = sec_spec['weight']
        sec_spec['pools'][0]['quantity'] = sec_spec['quantity']
        del sec_spec['quantity'], sec_spec['included-sections']

    for p in sec_spec['pools']:
        stdize_pool_spec(p, sec_spec)


def stdize_exam_spec(exam_spec):
    # do stuff to exam_spec
    for s in exam_spec['sections']:
        stdize_sec_spec(s, exam_spec)


def resolve_req(req, def_sec):
    return [ddef for ddef in def_sec if included(req, [ddef])]


def remaining_defs(pools):
    rem_p = []
    for secn, sec in pools.items():
        for typn, ty in sec.items():
            for typp in ty:
                if len(typp['questions']):
                    rem_p += [PoolName(secn)]
    return sorted(list(set(rem_p)))


def plan_exam(todo, pools, variants):
    plan = deepcopy(todo)
    req_pools = list(set([PoolName(str(x)) for sec in plan['sections'] for p in sec['pools'] for x in p['included-sections']]))
    req_pools.sort()

    for poolname in req_pools:
        rdefs = remaining_defs(pools)
        targs = resolve_req(poolname, rdefs)
        for t in targs:
            for sec in plan['sections']:
                plan_section(sec, t, pools, variants)

    return plan


def typeset_exam(plan):
    global tex

    tex.preamble.append(Package('graphicx'))
    tex.preamble.append(Package('tabularx'))
    tex.preamble.append(Package('inputenc'))
    tex.preamble.append(Package('needspace'))
    tex.preamble.append(Package('geometry', options={'letterpaper': None,
                                                     'tmargin': "1.0in",
                                                     'headsep': "0.25in",
                                                     'left': str(0.5*(8.5-textwidth))+'in',
                                                     'right': str(0.5*(8.5-textwidth))+'in',
                                                     'bottom': '0.6in',
                                                     'footskip': '0.15in'
                                                     }))
    tex.preamble.append(Package('babel', 'english'))

    tex.preamble.append(Package('floatrow'))
    #tex.preamble.append(Package('float'))
    #tex.preamble.append(Command('pointsinleftmargin'))
    #tex.preamble.append(Command('pointsinrightmargin'))

    if not args.no_continuation_headings:
        tex.preamble.append(Package('titlesec', options=['explicit']))
        tex.preamble.append(Package('everyshi'))
        tex.preamble.append(Package('etoolbox'))

        tex.preamble.append(NoEscape(r"""
        
        
\makeatletter

\titleformat{name=\section}
  {\normalfont\bfseries\Large}
  {\thesection}
  {1em}
  {\gdef\@section@title@{\thesection\quad#1 (continued)}#1}

\titleformat{name=\section,numberless}
  {\normalfont\bfseries\Large}
  {}
  {0pt}
  {\gdef\@section@title@{#1 (continued)}#1}

\titlespacing{\section}
  {0pt}
  {3ex plus 2ex minus 1ex}
  {1ex plus .5ex minus .5ex}

\let\@section@title@\relax% Sectional heading storage
\def\print@section@title@{%
  {\noindent\normalfont\bfseries\Large\@section@title@}\par\vspace{1ex plus .5ex minus .5ex}%
}
\EveryShipout{%
  \ifdim\pagetotal>\pagegoal% There is content overflow on this page
    \aftergroup\print@section@title@% Reprint/-insert sectional heading
  \fi%
}
\makeatother


"""))

    # title page and author info
    tex.preamble.append(Command('title', plan['title']))
    tex.preamble.append(Command('author', plan['author']))
    tex.preamble.append(Command('date', plan['test-date']))

    tex.append(make_coverpage(plan))

    tex.append(Command('setlength', arguments=[NoEscape(r'\fboxsep'), smallgap]))
    makeHeader(plan)

    with tex.create(TexEnvs.questions()):
        for sec in plan['sections']:
            try:
                typeset_section(sec)
            except PoolExhausted as e:
                print("Question pool exhausted while trying to make section: {}".format(sec), file=sys.stderr)
                raise e



def read_spec():
    global args
    with open(args.exam_spec, 'r') as f:
        spec = yaml.load(f, Loader=Loader)
    return spec


def walk_pool(nest, context={'section': None, 'type': None, 'value': None, 'prompt': None}, top=False):

    context = deepcopy(context)

    if isinstance(nest, dict):
        if 'section' in nest:
            context['section'] = nest['section']
        if 'type' in nest:
            context['type'] = nest['type']
        if 'prompt' in nest:
            context['prompt'] = nest['prompt']
        if 'value' in nest:
            context['value'] = nest['value']

    yield nest, context

    if isinstance(nest, list):
        for item in nest:
            yield from walk_pool(item, context=context, top=top)

    if isinstance(nest, dict):
        if 'questions' not in nest:
            for itn, item in nest.items():
                ccontext = deepcopy(context)

                if top and not ccontext['section']:
                    ccontext['section'] = itn

                elif not top and ccontext['section'] and not ccontext['type']:
                    ccontext['type'] = itn

                yield from walk_pool(item, context=ccontext, top=False)


def read_pools(poolpaths):

    pools = {}

    def handle_doc(doc):
        if doc:
            def handle_doc_dict(doc):
                assert isinstance(doc, dict)
                pools.update(doc)

            if isinstance(doc, dict):
                handle_doc_dict(doc)

            elif isinstance(doc, list):
                for v in doc:
                    if isinstance(v, dict):
                        if 'section' in v:
                            secn = v.pop('section')
                            handle_doc_dict({secn: v})

    def open_and_handle(f):
        try:
            with open(f, 'r') as f:
                for doc in yaml.load_all(f, Loader=UniqueKeyLoader):
                    handle_doc(doc)
        except DuplicateYAMLKeyError as e:
            print(e.args[0] + ' ({})'.format(e.args[1]), file=sys.stderr)

    if not isinstance(poolpaths, list):
        poolpaths = [poolpaths]

    for poolpaths in poolpaths:
        try:
            if os.path.isdir(poolpaths):
                poolpaths = Path(poolpaths)
                for root, dirs, files in os.walk(poolpaths):
                    for yf in [f for f in files if f.endswith('.yaml')]:
                        open_and_handle(os.path.join(root, yf))

            elif os.path.isfile(poolpaths):
                open_and_handle(poolpaths)

            else:
                raise FileNotFoundError

        except yaml.parser.ParserError as e:
            print("YAML syntax error:  ##########################################", file=sys.stderr)
            print('    {}:'.format(e.problem), file=sys.stderr)
            print('        {}'.format(e.problem_mark), file=sys.stderr)
            quit(1)

        except yaml.scanner.ScannerError as e:
            print("YAML syntax error:  ##########################################", file=sys.stderr)
            print('    {}:'.format(e.problem), file=sys.stderr)
            print('        {}'.format(e.problem_mark), file=sys.stderr)
            quit(1)

    return pools


# def rekey_lower(pools):
#     # if not isinstance(any_dict, dict):
#     #     raise TypeError('Can only use rekey_lower() on dicts')
#     # for k, v in any_dict.items():
#     #     if k != k.lower():
#     #         del any_dict[k]
#     #         any_dict[k.lower] = v
#     while True:
#         term = True
#         for _, _, _, item in walk_pool(pools, top=True):
#             if isinstance(item, dict):
#                 for k, v in item.items():
#                     if not isinstance(k, str):
#                         nk = str(k)
#                     else:
#                         nk = k
#                     if nk != nk.lower():
#                         nk = nk.lower()
#                     if nk != k:
#                         item[nk] = v
#                         del item[k]
#                         term=False
#                         break
#         if term:
#             break

def regularize_questions(question_bloc, context):

    result = []

    questions = question_bloc['questions']

    if questions is None:
        return None

    if not isinstance(questions, list):
        raise NotImplementedError

    for i, v in enumerate(questions):

        if v is None:
            continue

        if isinstance(v, list):
            raise NotImplementedError
            # del questions[i]
            # for vi, vv in v:
            #     questions.append(regularize_questions(vv, prompt, value))

        if isinstance(v, str):
            v = {'text': v}

        if isinstance(v, dict):
            if 'prompt' not in v:
                v['prompt'] = context['prompt']
            if 'value' not in v:
                v['value'] = context['value']
            if 'text' not in v:
                v['text'] = ''
            if 'graphic' in v:
                if not os.path.isfile(v['graphic']):
                    v['graphic'] = graphics_directory / v['graphic']
                    if not v['graphic'].is_file():
                        print('could not find graphics file: {}'.format(v['graphic']), file=sys.stderr)
                        exit(1)
            if 'alternates' in v:
                v['variants'] = v['alternates']
                del v['alternates']
        result += [v]

    return result


def regularize_pools(pools, prompt=None, value=default_value):

    rpool={}

    def insert_item(secn, typen, item):
        data = deepcopy(item)

        data['section'] = str(secn).lower()
        data['question-type'] = parse_type(typen)

        if data['section'] in rpool:
            if data['question-type'] in rpool[data['section']]:
                rpool[data['section']][data['question-type']].append(data)
            else:
                rpool[data['section']].update({data['question-type']: [data]})
        else:
            rpool.update({data['section']: {data['question-type']: [data]}})

    if pools:

        for item, context in walk_pool(pools, top=True):
            if item and context['section'] and context['type'] and 'questions' in item:
                insert_item(context['section'], context['type'], item)

        pools = rpool
        prompt = None
        value = None

        for item, context in walk_pool(pools, top=True):
            if isinstance(item, dict):
                if 'questions' in item and item['questions']:
                    item['questions'] = regularize_questions(item, context)

    return pools


def python_api_plan(clargs):

    global graphics_directory, args, tex

    p = argparse.ArgumentParser(add_help=True)
    p.add_argument('exam_spec')
    p.add_argument('--no-continuation-headings', '-c', action='store_true')
    args, unkn = p.parse_known_args(clargs)

    spec = read_spec()
    if 'question-pools' in spec:
        qpools = spec['question-pools']
    if 'graphics-path' in spec:
        graphics_directory = Path(spec['graphics-path'])
    if 'default-value' in spec:
        default_value = spec['default-value']
    if 'versions' in spec:
        n_versions = spec['versions']
    else:
        n_versions = 1

    stdize_exam_spec(spec)
    pools = read_pools(qpools)
    pools = regularize_pools(pools)
    prune_pools(pools)

    variant_choices = {}

    versions = [None]*n_versions

    for i in range(n_versions):

        activepool = deepcopy(pools)

        if 'graphics-dir' in spec:
            graphics_directory = Path(spec['graphics-dir'])

        try:
            versions[i] = plan_exam(spec, activepool, variant_choices)
            # tex = pylatex.Document(documentclass='exam', document_options=['12pt', 'addpoints'])
            # typeset_exam(plan)
            # tex.generate_pdf('_'.join([spec['title'], str(i)]), clean_tex=False, clean=False)
            # tex.generate_pdf('_'.join([spec['title'], str(i)]), clean_tex=False, clean=True)

        except PoolExhausted:
            try:
                print("The Question pool was exhausted while trying to make test: {}".format(plan['title']),
                      file=sys.stderr)
            except KeyError:
                try:
                    print("The Question pool was exhausted while trying to make test: {}".format(plan['Title']),
                          file=sys.stderr)
                except KeyError:
                    print("The Question pool was exhausted while trying to make test: {}".format(plan), file=sys.stderr)

            exit(1)

        except CalledProcessError:
            print('The LaTeX file was created, but it\'s compilation failed.', file=sys.stderr)
            exit(1)

    return versions


def main():
    global graphics_directory, args, tex

    p = argparse.ArgumentParser(add_help=True)
    p.add_argument('exam_spec')
    p.add_argument('--no-continuation-headings', '-c', action='store_true')
    args, unkn = p.parse_known_args()

    spec = read_spec()
    if 'question-pools' in spec:
        qpools = spec['question-pools']
    if 'graphics-path' in spec:
        graphics_directory = Path(spec['graphics-path'])
    if 'default-value' in spec:
        default_value = spec['default-value']
    if 'versions' in spec:
        n_versions = spec['versions']
    else:
        n_versions = 1

    stdize_exam_spec(spec)
    pools = read_pools(qpools)
    pools = regularize_pools(pools)
    prune_pools(pools)

    variant_choices = {}

    for i in range(n_versions):

        activepool = deepcopy(pools)

        if 'graphics-dir' in spec:
            graphics_directory = Path(spec['graphics-dir'])

        try:
            plan = plan_exam(spec, activepool, variant_choices)
            tex = pylatex.Document(documentclass='exam', document_options=['12pt', 'addpoints'])
            typeset_exam(plan)
            tex.generate_pdf('_'.join([spec['title'], str(i)]), clean_tex=False, clean=False)
            tex.generate_pdf('_'.join([spec['title'], str(i)]), clean_tex=False, clean=True)

        except PoolExhausted:
            try:
                print("The Question pool was exhausted while trying to make test: {}".format(plan['title']),
                      file=sys.stderr)
            except KeyError:
                try:
                    print("The Question pool was exhausted while trying to make test: {}".format(plan['Title']),
                          file=sys.stderr)
                except KeyError:
                    print("The Question pool was exhausted while trying to make test: {}".format(plan), file=sys.stderr)

            exit(1)

        except CalledProcessError:
            print('The LaTeX file was created, but it\'s compilation failed.', file=sys.stderr)
            exit(1)


if __name__ == '__main__':
    exit(main())
