# yatter

## Yatter makes test and exam creation easy.  

- Yatter requires you to create two simple YAML files:
    1. at least one quesiton pool
    2. One short test specification


- Yatter supports many types of question in the pools:
    1. True-False
    2. Multiple Choice
    3. Short Answer
    4. Flexible "long form" (includes graphics, etc.)

- Yatter extends the popular `exam.cls` LaTeX class.

Use Yatter to make your next quiz/test/exam!
